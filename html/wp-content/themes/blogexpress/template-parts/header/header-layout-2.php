<?php
/**
 * Header Layout 2
 *
 * @package BlogExpress
 */
$blogexpress_default = blogexpress_get_default_theme_options();
?>

<header id="site-header" class="site-header-layout header-layout-2" role="banner">
    <div class="header-navbar">
        <div class="wrapper">
            <div class="wrapper-inner">
                <div class="navbar-item navbar-item-left">
                    <div class="header-titles">
                        <?php
                        // Site title or logo.
                        blogexpress_site_logo();
                        // Site description.
                        blogexpress_site_description();
                        ?>
                    </div><!-- .header-titles -->
                </div>

                <div class="navbar-item navbar-item-right">

                    <div class="site-navigation">

                        <div class="wrapper">
                            <nav class="primary-menu-wrapper" aria-label="<?php esc_attr_e('Horizontal', 'blogexpress'); ?>" role="navigation">
                                <ul class="primary-menu theme-menu">

                                    <?php
                                    if( has_nav_menu('blogexpress-primary-menu') ){

                                        wp_nav_menu(
                                            array(
                                                'container' => '',
                                                'items_wrap' => '%3$s',
                                                'theme_location' => 'blogexpress-primary-menu',
                                                'walker' => new blogexpress\BlogExpress_Walkernav(),
                                            )
                                        );

                                    }else{

                                        wp_list_pages(
                                            array(
                                                'match_menu_classes' => true,
                                                'show_sub_menu_icons' => true,
                                                'title_li' => false,
                                                'walker' => new BlogExpress_Walker_Page(),
                                            )
                                        );

                                    } ?>

                                </ul>
                            </nav><!-- .primary-menu-wrapper -->
                        </div>
                    </div><!-- .site-navigation -->

                    <div class="navbar-controls twp-hide-js">



                        <button type="button" class="navbar-control navbar-control-offcanvas">
                             <span class="navbar-control-trigger" tabindex="-1">
                                <?php blogexpress_the_theme_svg('menu'); ?>
                             </span>
                        </button>


                        <button type="button" class="navbar-control navbar-control-search">
                            <span class="navbar-control-trigger" tabindex="-1">
                                <?php blogexpress_the_theme_svg('search'); ?>
                            </span>
                        </button>
                         
                    </div>

                </div>
            </div>
        </div>
    </div>

</header><!-- #site-header -->
