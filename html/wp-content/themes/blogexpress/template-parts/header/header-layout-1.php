<?php
/**
 * Header Layout 1
 *
 * @package BlogExpress
 */
$blogexpress_default = blogexpress_get_default_theme_options();
?>

<header id="site-header" class="site-header-layout header-layout-1" role="banner">
    <div class="header-navbar">
        <div class="wrapper">
            <div class="wrapper-inner">
                <div class="navbar-item navbar-item-left">
                    <div class="header-titles">
                        <?php
                        blogexpress_site_logo();
                        blogexpress_site_description();
                        ?>
                    </div>
                </div>

                <div class="navbar-item navbar-item-right">
                    <div class="navbar-controls twp-hide-js">
                    <?php get_search_form(); ?>

                    <?php blogexpress_category_pin_posts_link(); ?>
                    </div>
                </div>
                        
            </div>
        </div>
    </div>

    <div class="site-navigation">
        <div class="wrapper">
            <div class="wrapper-inner">
                <div class="navbar-item navbar-item-left">
                    <div class="main-nav-controls twp-hide-js">

                        <button type="button" class="navbar-control navbar-control-offcanvas">
                             <span class="navbar-control-trigger" tabindex="-1">
                                <?php blogexpress_the_theme_svg('menu'); ?>
                             </span>
                        </button>

                        <nav class="primary-menu-wrapper" aria-label="<?php esc_attr_e('Horizontal', 'blogexpress'); ?>" role="navigation">
                            <ul class="primary-menu theme-menu">

                                <?php
                                if( has_nav_menu('blogexpress-primary-menu') ){

                                    wp_nav_menu(
                                        array(
                                            'container' => '',
                                            'items_wrap' => '%3$s',
                                            'theme_location' => 'blogexpress-primary-menu',
                                            'walker' => new blogexpress\BlogExpress_Walkernav(),
                                        )
                                    );

                                }else{

                                    wp_list_pages(
                                        array(
                                            'match_menu_classes' => true,
                                            'show_sub_menu_icons' => true,
                                            'title_li' => false,
                                            'walker' => new BlogExpress_Walker_Page(),
                                        )
                                    );

                                } ?>

                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="navbar-item navbar-item-right">

                    <div class="navbar-controls twp-hide-js">

                        <?php if (has_nav_menu('blogexpress-social-menu')) { ?>
                            <div id="main-social-nav" class="main-social-navigation">
                                <?php wp_nav_menu(array(
                                    'theme_location' => 'blogexpress-social-menu',
                                    'link_before' => '<span class="screen-reader-text">',
                                    'link_after' => '</span>',
                                    'container' => 'div',
                                    'container_class' => 'social-menu',
                                    'depth' => 1,
                                )); ?>
                            </div>
                        <?php } ?>

                        <button type="button" class="navbar-control navbar-control-search">
                            <span class="navbar-control-trigger" tabindex="-1">
                                <?php blogexpress_the_theme_svg('search'); ?>
                            </span>
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>

</header>