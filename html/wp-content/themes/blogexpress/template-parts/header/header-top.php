<?php
/**
 * Header Top
 *
 * @package BlogExpress
 */
$blogexpress_default = blogexpress_get_default_theme_options();

?>

<div class="site-topbar">
    <?php
    $header_ad_image = get_theme_mod('header_ad_image');
    $header_ad_image_link = get_theme_mod('header_ad_image_link');

    if ($header_ad_image) { ?>

        <div class="navbar-item navbar-item-right">
            <a href="<?php echo esc_url($header_ad_image_link); ?>">
                <img src="<?php echo esc_url($header_ad_image); ?>"
                     alt="<?php esc_attr_e('Header Image', 'blogexpress'); ?>"
                     title="<?php esc_attr_e('Header Image', 'blogexpress'); ?>">
            </a>
        </div>

    <?php } ?>
    <div class="wrapper">
        <div class="wrapper-inner">

            <div class="navbar-item navbar-item-left">
                <?php if (has_nav_menu('blogexpress-top-menu')) { ?>

                    <div id="top-nav-header" class="header-item header-top-navigation">
                        <?php wp_nav_menu(array(
                            'theme_location' => 'blogexpress-top-menu',
                            'container' => 'div',
                            'container_class' => 'top-menu',
                            'depth' => 1,
                        )); ?>
                    </div>

                <?php } ?>
            </div>

            <div class="navbar-item navbar-item-right">
                
                <?php
                blogexpress_category_header_info();
                $blogexpress_default = blogexpress_get_default_theme_options();
                $blogexpress_header_layout = get_theme_mod( 'blogexpress_header_layout', $blogexpress_default['blogexpress_header_layout'] );
                if( $blogexpress_header_layout == 'layout-2' ){
                    blogexpress_category_pin_posts_link();
                }
                ?>

            </div>

        </div>
    </div>

</div>
