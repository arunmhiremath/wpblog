<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage BlogExpress
 * @since 1.0.0
 */
$blogexpress_default = blogexpress_get_default_theme_options();
$ed_post_read_later = get_theme_mod('ed_post_read_later',$blogexpress_default['ed_post_read_later']);
$image_size = 'large';
global $blogexpress_order_class_1, $blogexpress_order_class_2; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('theme-article-post theme-article-animate'); ?>>
    <div class="column-row">

        <div class="column column-8 column-sm-12  <?php echo esc_attr($blogexpress_order_class_1); ?>">
            <div class="theme-tilt">
                <div class="entry-thumbnail">

                    <?php
                    if (is_search() || is_archive() || is_front_page()) {

                        $featured_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
                        $featured_image = isset( $featured_image[0] ) ? $featured_image[0] : ''; ?>
                        <div class="post-thumbnail data-bg data-bg-big twp-ani-control theme-reveal"
                             data-background="<?php echo esc_url( $featured_image ); ?>">
                            <a href="<?php the_permalink(); ?>" class="theme-image-responsive" tabindex="0"></a>
                        </div>

                        <?php
                    } else {

                        blogexpress_post_thumbnail($image_size);

                    }
                    blogexpress_post_format_icon(); ?>
                </div>

            </div>

        </div>

        <div class="column column-4 column-sm-12 <?php echo esc_attr($blogexpress_order_class_2); ?>">

            <div class="entry-meta-top">
                <div class="entry-meta">
                    <?php blogexpress_entry_footer($cats = true, $tags = false, $edits = false); ?>
                </div>
            </div>

            <header class="entry-header">

                <h2 class="entry-title entry-title-big">

                    <a href="<?php the_permalink(); ?>" rel="bookmark" class="theme-animate-up animate-theme-reveal-up">
                        <span><?php the_title(); ?></span>
                    </a>


                </h2>

            </header>


            <div class="entry-content">

                <?php
                if (has_excerpt()) {

                    the_excerpt();

                } else {

                    echo '<p>';
                    echo esc_html(wp_trim_words(get_the_content(), 25, '...'));
                    echo '</p>';
                }

                wp_link_pages(array(
                    'before' => '<div class="page-links">' . esc_html__('Pages:', 'blogexpress'),
                    'after' => '</div>',
                )); ?>

            </div>

            <div class="entry-footer">

                <div class="entry-meta">
                    <?php
                    blogexpress_posted_by_avatar( $date = true);
                    ?>
                </div>

                <?php if ( class_exists('Booster_Extension_Class') && $ed_post_read_later ): echo do_shortcode('[be-pp]'); endif; ?>

            </div>

            <a href="<?php the_permalink(); ?>" rel="bookmark" class="btn-fancy">
                <span class="btn-arrow"></span>
                <span class="btn-arrow-text"><?php esc_html_e('Continue Reading', 'blogexpress'); ?></span>
            </a>

        </div>

    </div>
</article>