<?php
/**
* Posts Settings.
*
* @package BlogExpress
*/

$blogexpress_default = blogexpress_get_default_theme_options();

// Single Post Section.
$wp_customize->add_section( 'posts_settings',
	array(
	'title'      => esc_html__( 'Posts Settings', 'blogexpress' ),
	'priority'   => 35,
	'capability' => 'edit_theme_options',
	'panel'      => 'theme_option_panel',
	)
);

$wp_customize->add_setting('ed_post_author',
    array(
        'default' => $blogexpress_default['ed_post_author'],
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'blogexpress_sanitize_checkbox',
    )
);
$wp_customize->add_control('ed_post_author',
    array(
        'label' => esc_html__('Enable Posts Author', 'blogexpress'),
        'section' => 'posts_settings',
        'type' => 'checkbox',
    )
);

$wp_customize->add_setting('ed_post_date',
    array(
        'default' => $blogexpress_default['ed_post_date'],
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'blogexpress_sanitize_checkbox',
    )
);
$wp_customize->add_control('ed_post_date',
    array(
        'label' => esc_html__('Enable Posts Date', 'blogexpress'),
        'section' => 'posts_settings',
        'type' => 'checkbox',
    )
);

$wp_customize->add_setting('ed_post_category',
    array(
        'default' => $blogexpress_default['ed_post_category'],
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'blogexpress_sanitize_checkbox',
    )
);
$wp_customize->add_control('ed_post_category',
    array(
        'label' => esc_html__('Enable Posts Category', 'blogexpress'),
        'section' => 'posts_settings',
        'type' => 'checkbox',
    )
);

$wp_customize->add_setting('ed_post_tags',
    array(
        'default' => $blogexpress_default['ed_post_tags'],
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'blogexpress_sanitize_checkbox',
    )
);
$wp_customize->add_control('ed_post_tags',
    array(
        'label' => esc_html__('Enable Posts Tags', 'blogexpress'),
        'section' => 'posts_settings',
        'type' => 'checkbox',
    )
);