<?php
/**
* Top Header Options.
*
* @package BlogExpress
*/

$blogexpress_default = blogexpress_get_default_theme_options();

// Header Advertise Area Section.
$wp_customize->add_section( 'top_header_setting',
	array(
	'title'      => esc_html__( 'Top Header Settings', 'blogexpress' ),
	'priority'   => 10,
	'capability' => 'edit_theme_options',
	'panel'      => 'theme_option_panel',
	)
);

// Enable Disable Search.
$wp_customize->add_setting('blogexpress_header_top_ed',
    array(
        'default' => $blogexpress_default['blogexpress_header_top_ed'],
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'blogexpress_sanitize_checkbox',
    )
);
$wp_customize->add_control('blogexpress_header_top_ed',
    array(
        'label' => esc_html__('Enable Header Top', 'blogexpress'),
        'section' => 'top_header_setting',
        'type' => 'checkbox',
    )
);

$wp_customize->add_setting('top_bar_phone',
    array(
        'default' => '',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    )
);
$wp_customize->add_control('top_bar_phone',
    array(
        'label' => esc_html__('Phone Number', 'blogexpress'),
        'section' => 'top_header_setting',
        'type' => 'text',
    )
);

$wp_customize->add_setting('top_bar_email',
    array(
        'default' => '',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_email',
    )
);
$wp_customize->add_control('top_bar_email',
    array(
        'label' => esc_html__('Email Address', 'blogexpress'),
        'section' => 'top_header_setting',
        'type' => 'text',
    )
);

$wp_customize->add_setting('top_bar_location',
    array(
        'default' => '',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    )
);
$wp_customize->add_control('top_bar_location',
    array(
        'label' => esc_html__('Location', 'blogexpress'),
        'section' => 'top_header_setting',
        'type' => 'text',
    )
);

$wp_customize->add_setting('top_bar_location_url',
    array(
        'default' => '',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'esc_url_raw',
    )
);
$wp_customize->add_control('top_bar_location_url',
    array(
        'label' => esc_html__('Location URL', 'blogexpress'),
        'section' => 'top_header_setting',
        'type' => 'text',
    )
);

$wp_customize->add_setting('header_ad_image',
    array(
        'default' => '',
        'sanitize_callback' => 'esc_url_raw'
    )
);
$wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize,
    'header_ad_image',
        array(
            'label'      => esc_html__( 'Top Header AD Image', 'blogexpress' ),
            'section'    => 'top_header_setting',
        )
    )
);

$wp_customize->add_setting('header_ad_image_link',
    array(
        'default' => '',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'esc_url_raw',
    )
);
$wp_customize->add_control('header_ad_image_link',
    array(
        'label' => esc_html__('AD Image Link Search', 'blogexpress'),
        'section' => 'top_header_setting',
        'type' => 'text',
    )
);
