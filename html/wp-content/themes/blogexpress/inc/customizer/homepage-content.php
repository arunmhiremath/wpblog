<?php
/**
* Header Banner Options.
*
* @package BlogExpress
*/

$blogexpress_default = blogexpress_get_default_theme_options();
$blogexpress_post_category_list = blogexpress_post_category_list();

$wp_customize->add_section( 'header_banner_setting',
    array(
    'title'      => esc_html__( 'Slider Banner Settings', 'blogexpress' ),
    'priority'   => 10,
    'capability' => 'edit_theme_options',
    'panel'      => 'theme_home_pannel',
    )
);

$wp_customize->add_setting('ed_header_banner',
    array(
        'default' => $blogexpress_default['ed_header_banner'],
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'blogexpress_sanitize_checkbox',
    )
);
$wp_customize->add_control('ed_header_banner',
    array(
        'label' => esc_html__('Enable Slider Banner', 'blogexpress'),
        'section' => 'header_banner_setting',
        'type' => 'checkbox',
    )
);

$wp_customize->add_setting( 'blogexpress_header_banner_cat',
    array(
    'default'           => '',
    'capability'        => 'edit_theme_options',
    'sanitize_callback' => 'blogexpress_sanitize_select',
    )
);
$wp_customize->add_control( 'blogexpress_header_banner_cat',
    array(
    'label'       => esc_html__( 'Slider Post Category', 'blogexpress' ),
    'section'     => 'header_banner_setting',
    'type'        => 'select',
    'choices'     => $blogexpress_post_category_list,
    )
);

$wp_customize->add_section( 'header_carousel_setting',
    array(
    'title'      => esc_html__( 'You May Have missed Section Settings', 'blogexpress' ),
    'priority'   => 10,
    'capability' => 'edit_theme_options',
    'panel'      => 'theme_home_pannel',
    )
);

$wp_customize->add_setting('ed_carousel_section',
    array(
        'default' => $blogexpress_default['ed_carousel_section'],
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'blogexpress_sanitize_checkbox',
    )
);
$wp_customize->add_control('ed_carousel_section',
    array(
        'label' => esc_html__('Enable Section', 'blogexpress'),
        'section' => 'header_carousel_setting',
        'type' => 'checkbox',
    )
);

$wp_customize->add_setting( 'blogexpress_carousel_section_cat',
    array(
    'default'           => '',
    'capability'        => 'edit_theme_options',
    'sanitize_callback' => 'blogexpress_sanitize_select',
    )
);
$wp_customize->add_control( 'blogexpress_carousel_section_cat',
    array(
    'label'       => esc_html__( ' Section Post Category', 'blogexpress' ),
    'section'     => 'header_carousel_setting',
    'type'        => 'select',
    'choices'     => $blogexpress_post_category_list,
    )
);

$wp_customize->add_setting('blogexpress_carousel_section_title',
    array(
        'default' => $blogexpress_default['blogexpress_carousel_section_title'],
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    )
);

$wp_customize->add_control('blogexpress_carousel_section_title',
    array(
        'label' => esc_html__('Section Title', 'blogexpress'),
        'section' => 'header_carousel_setting',
        'type' => 'text',
    )
);

$wp_customize->add_section( 'header_category_setting',
    array(
    'title'      => esc_html__( 'Category Carousel Settings', 'blogexpress' ),
    'priority'   => 10,
    'capability' => 'edit_theme_options',
    'panel'      => 'theme_home_pannel',
    )
);

$wp_customize->add_setting('ed_category_section',
    array(
        'default' => $blogexpress_default['ed_category_section'],
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'blogexpress_sanitize_checkbox',
    )
);
$wp_customize->add_control('ed_category_section',
    array(
        'label' => esc_html__('Enable Category Section', 'blogexpress'),
        'section' => 'header_category_setting',
        'type' => 'checkbox',
    )
);

for ($x = 1; $x <= 15; $x++) {

    $wp_customize->add_setting( 'blogexpress_category_cat_'.$x,
        array(
        'default'           => '',
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'blogexpress_sanitize_select',
        )
    );
    $wp_customize->add_control( 'blogexpress_category_cat_'.$x,
        array(
        'label'       => esc_html__( 'Category ', 'blogexpress' ).$x,
        'section'     => 'header_category_setting',
        'type'        => 'select',
        'choices'     => $blogexpress_post_category_list,
        )
    );

}