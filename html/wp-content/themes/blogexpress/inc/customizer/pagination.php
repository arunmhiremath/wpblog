<?php
/**
 * Pagination Settings
 *
 * @package BlogExpress
 */

$blogexpress_default = blogexpress_get_default_theme_options();

// Pagination Section.
$wp_customize->add_section( 'blogexpress_pagination_section',
	array(
	'title'      => esc_html__( 'Pagination Settings', 'blogexpress' ),
	'priority'   => 20,
	'capability' => 'edit_theme_options',
	'panel'		 => 'theme_option_panel',
	)
);

// Pagination Layout Settings
$wp_customize->add_setting( 'blogexpress_pagination_layout',
	array(
	'default'           => $blogexpress_default['blogexpress_pagination_layout'],
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	)
);
$wp_customize->add_control( 'blogexpress_pagination_layout',
	array(
	'label'       => esc_html__( 'Pagination Method', 'blogexpress' ),
	'section'     => 'blogexpress_pagination_section',
	'type'        => 'select',
	'choices'     => array(
		'next-prev' => esc_html__('Next/Previous Method','blogexpress'),
		'numeric' => esc_html__('Numeric Method','blogexpress'),
		'load-more' => esc_html__('Ajax Load More Button','blogexpress'),
		'auto-load' => esc_html__('Ajax Auto Load','blogexpress'),
	),
	)
);