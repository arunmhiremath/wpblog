<?php
/**
* Color Settings.
*
* @package BlogExpress
*/

$blogexpress_default = blogexpress_get_default_theme_options();

$wp_customize->add_setting( 'blogexpress_primary_color',
    array(
    'default'           => $blogexpress_default['blogexpress_primary_color'],
    'capability'        => 'edit_theme_options',
    'sanitize_callback' => 'sanitize_hex_color',
    )
);
$wp_customize->add_control( 
    new WP_Customize_Color_Control( 
    $wp_customize, 
    'blogexpress_primary_color',
    array(
        'label'      => esc_html__( 'Text Color', 'blogexpress' ),
        'section'    => 'colors',
        'settings'   => 'blogexpress_primary_color',
    ) ) 
);

$wp_customize->add_setting( 'blogexpress_border_color',
    array(
    'default'           => $blogexpress_default['blogexpress_border_color'],
    'capability'        => 'edit_theme_options',
    'sanitize_callback' => 'sanitize_hex_color',
    )
);
$wp_customize->add_control( 
    new WP_Customize_Color_Control( 
    $wp_customize, 
    'blogexpress_border_color',
    array(
        'label'      => esc_html__( 'Border Color', 'blogexpress' ),
        'section'    => 'colors',
        'settings'   => 'blogexpress_border_color',
    ) ) 
);