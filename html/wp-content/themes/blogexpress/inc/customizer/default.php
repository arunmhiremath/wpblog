<?php
/**
 * Default Values.
 *
 * @package BlogExpress
 */

if ( ! function_exists( 'blogexpress_get_default_theme_options' ) ) :

	/**
	 * Get default theme options
	 *
	 * @since 1.0.0
	 *
	 * @return array Default theme options.
	 */
	function blogexpress_get_default_theme_options() {

		$blogexpress_defaults = array();
		
		// Options.
		$blogexpress_defaults['blogexpress_pagination_layout']					= 'numeric';
		$blogexpress_defaults['footer_column_layout'] 						= 3;
		$blogexpress_defaults['footer_copyright_text'] 						= esc_html__( 'All rights reserved.', 'blogexpress' );
                $blogexpress_defaults['ed_preloader']                                                        = 0;
		$blogexpress_defaults['blogexpress_header_top_ed'] 							= 1;
		$blogexpress_defaults['ed_related_post']                				= 1;
                $blogexpress_defaults['related_post_title']             				= esc_html__('Similar Articles','blogexpress');
                $blogexpress_defaults['blogexpress_carousel_section_title']             	= esc_html__('You may have missed','blogexpress');
                $blogexpress_defaults['twp_navigation_type']              			= 'theme-normal-navigation';
                $blogexpress_defaults['ed_post_author']                				= 1;
                $blogexpress_defaults['ed_post_date']                				= 1;
                $blogexpress_defaults['ed_post_category']                			= 1;
                $blogexpress_defaults['ed_post_tags']                				= 1;
                $blogexpress_defaults['ed_floating_next_previous_nav']               = 1;
                $blogexpress_defaults['blogexpress_header_layout']               			= 'layout-1';
                $blogexpress_defaults['ed_header_banner']               				= 0;
                $blogexpress_defaults['ed_carousel_section']               			= 0;
                $blogexpress_defaults['ed_category_section']               			= 0;
                $blogexpress_defaults['blogexpress_background_color']               		= '#fff';
                $blogexpress_defaults['blogexpress_primary_color']               			= '#000';
                $blogexpress_defaults['blogexpress_border_color']               		= '#000';
                $blogexpress_defaults['default_tertiary_color'] 						= '#2568ef';
                $blogexpress_defaults['blogexpress_background_color_dark']               	= '#000';
                $blogexpress_defaults['blogexpress_primary_color_dark']               	= '#fff';
                $blogexpress_defaults['blogexpress_border_color_dark']               	= '#fff';
                $blogexpress_defaults['dark_tertiary_color'] 						= '#2568ef';
                $blogexpress_defaults['ed_post_read_later']                                             = 1;

		// Pass through filter.
		$blogexpress_defaults = apply_filters( 'blogexpress_filter_default_theme_options', $blogexpress_defaults );

		return $blogexpress_defaults;

	}

endif;
