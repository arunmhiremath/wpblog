<?php
/**
* Header Options.
*
* @package BlogExpress
*/

$blogexpress_default = blogexpress_get_default_theme_options();

// Header Advertise Area Section.
$wp_customize->add_section( 'main_header_setting',
	array(
	'title'      => esc_html__( 'Header Settings', 'blogexpress' ),
	'priority'   => 10,
	'capability' => 'edit_theme_options',
	'panel'      => 'theme_option_panel',
	)
);

// Archive Layout.
$wp_customize->add_setting(
    'blogexpress_header_layout',
    array(
        'default'           => $blogexpress_default['blogexpress_header_layout'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'blogexpress_sanitize_header_layout'
    )
);
$wp_customize->add_control(
    new BlogExpress_Custom_Radio_Image_Control( 
        $wp_customize,
        'blogexpress_header_layout',
        array(
            'settings'      => 'blogexpress_header_layout',
            'section'       => 'main_header_setting',
            'label'         => esc_html__( 'Header Layout', 'blogexpress' ),
            'choices'       => array(
                'layout-1'  => get_template_directory_uri() . '/assets/images/header-layout-1.png',
                'layout-2'  => get_template_directory_uri() . '/assets/images/header-layout-2.png',
            )
        )
    )
);