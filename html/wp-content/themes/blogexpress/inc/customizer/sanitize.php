<?php
/**
* Custom Functions.
*
* @package BlogExpress
*/

if( !function_exists( 'blogexpress_sanitize_sidebar_option' ) ) :

    // Sidebar Option Sanitize.
    function blogexpress_sanitize_sidebar_option( $blogexpress_input ){

        $blogexpress_metabox_options = array( 'global-sidebar','left-sidebar','right-sidebar','no-sidebar' );
        if( in_array( $blogexpress_input,$blogexpress_metabox_options ) ){

            return $blogexpress_input;

        }

        return;

    }

endif;

if( !function_exists( 'blogexpress_sanitize_single_pagination_layout' ) ) :

    // Sidebar Option Sanitize.
    function blogexpress_sanitize_single_pagination_layout( $blogexpress_input ){

        $blogexpress_single_pagination = array( 'no-navigation','theme-normal-navigation','ajax-next-post-load' );
        if( in_array( $blogexpress_input,$blogexpress_single_pagination ) ){

            return $blogexpress_input;

        }

        return;

    }

endif;

if( !function_exists( 'blogexpress_sanitize_archive_layout' ) ) :

    // Sidebar Option Sanitize.
    function blogexpress_sanitize_archive_layout( $blogexpress_input ){

        $blogexpress_archive_option = array( 'default','full','grid','masonry' );
        if( in_array( $blogexpress_input,$blogexpress_archive_option ) ){

            return $blogexpress_input;

        }

        return;

    }

endif;

if( !function_exists( 'blogexpress_sanitize_header_layout' ) ) :

    // Sidebar Option Sanitize.
    function blogexpress_sanitize_header_layout( $blogexpress_input ){

        $blogexpress_header_options = array( 'layout-1','layout-2' );
        if( in_array( $blogexpress_input,$blogexpress_header_options ) ){

            return $blogexpress_input;

        }

        return;

    }

endif;

if( !function_exists( 'blogexpress_sanitize_single_post_layout' ) ) :

    // Single Layout Option Sanitize.
    function blogexpress_sanitize_single_post_layout( $blogexpress_input ){

        $blogexpress_single_layout = array( 'layout-1','layout-2' );
        if( in_array( $blogexpress_input,$blogexpress_single_layout ) ){

            return $blogexpress_input;

        }

        return;

    }

endif;

if ( ! function_exists( 'blogexpress_sanitize_checkbox' ) ) :

	/**
	 * Sanitize checkbox.
	 */
	function blogexpress_sanitize_checkbox( $blogexpress_checked ) {

		return ( ( isset( $blogexpress_checked ) && true === $blogexpress_checked ) ? true : false );

	}

endif;


if ( ! function_exists( 'blogexpress_sanitize_select' ) ) :

    /**
     * Sanitize select.
     */
    function blogexpress_sanitize_select( $blogexpress_input, $blogexpress_setting ) {

        // Ensure input is a slug.
        $blogexpress_input = sanitize_text_field( $blogexpress_input );

        // Get list of choices from the control associated with the setting.
        $choices = $blogexpress_setting->manager->get_control( $blogexpress_setting->id )->choices;

        // If the input is a valid key, return it; otherwise, return the default.
        return ( array_key_exists( $blogexpress_input, $choices ) ? $blogexpress_input : $blogexpress_setting->default );

    }

endif;