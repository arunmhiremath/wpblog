<?php
/**
* Preloader Options.
*
* @package BlogExpress
*/

$blogexpress_default = blogexpress_get_default_theme_options();

$wp_customize->add_section( 'preloader_setting',
	array(
	'title'      => esc_html__( 'Preloader Settings', 'blogexpress' ),
	'priority'   => 10,
	'capability' => 'edit_theme_options',
	'panel'      => 'theme_option_panel',
	)
);

$wp_customize->add_setting('ed_preloader',
    array(
        'default' => $blogexpress_default['ed_preloader'],
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'blogexpress_sanitize_checkbox',
    )
);
$wp_customize->add_control('ed_preloader',
    array(
        'label' => esc_html__('Enable Preloader', 'blogexpress'),
        'section' => 'preloader_setting',
        'type' => 'checkbox',
    )
);
