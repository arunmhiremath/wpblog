<?php
/**
* Sidebar Metabox.
*
* @package BlogExpress
*/

add_action( 'add_meta_boxes', 'blogexpress_metabox' );

if( ! function_exists( 'blogexpress_metabox' ) ):


    function blogexpress_metabox() {
        
        add_meta_box(
            'twp-custom-metabox',
            esc_html__( 'Single Post Settings', 'blogexpress' ),
            'blogexpress_post_metafield_callback',
            'post', 
            'normal', 
            'high'
        );
    }

endif;

/**
 * Callback function for post option.
*/
if( ! function_exists( 'blogexpress_post_metafield_callback' ) ):
    
	function blogexpress_post_metafield_callback() {

		global $post;
        $post_type = get_post_type($post->ID);
		wp_nonce_field( basename( __FILE__ ), 'blogexpress_post_meta_nonce' ); ?>
        
        <div class="metabox-main-block">

            <div class="metabox-navbar">
                <ul>

                    <li>
                        <a id="metabox-navbar-appearance" class="metabox-navbar-active" href="javascript:void(0)">

                            <?php esc_html_e('Appearance Settings', 'blogexpress'); ?>

                        </a>
                    </li>

                    <?php
                    if( class_exists('Booster_Extension_Class') ){ ?>

                        <li>
                            <a id="twp-tab-booster" href="javascript:void(0)">

                                <?php esc_html_e('Booster Extension Settings', 'blogexpress'); ?>

                            </a>
                        </li>

                    <?php } ?>

                </ul>
            </div>

            <div class="twp-tab-content">

                <?php if( $post_type == 'post' ): ?>

                    <div id="metabox-navbar-appearance-content" class="metabox-content-wrap metabox-content-wrap-active">

                         <div class="metabox-opt-panel">

                            <h3 class="meta-opt-title"><?php esc_html_e('Navigation Setting','blogexpress'); ?></h3>

                            <?php
                            $twp_disable_ajax_load_next_post = esc_attr( get_post_meta($post->ID, 'twp_disable_ajax_load_next_post', true) ); ?>
                            <div class="metabox-opt-wrap metabox-opt-wrap-alt">

                                <label><b><?php esc_html_e( 'Navigation Type','blogexpress' ); ?></b></label>

                                <select name="twp_disable_ajax_load_next_post">

                                    <option <?php if( $twp_disable_ajax_load_next_post == '' || $twp_disable_ajax_load_next_post == 'global-layout' ){ echo 'selected'; } ?> value="global-layout"><?php esc_html_e('Global Layout','blogexpress'); ?></option>
                                    <option <?php if( $twp_disable_ajax_load_next_post == 'no-navigation' ){ echo 'selected'; } ?> value="no-navigation"><?php esc_html_e('Disable Navigation','blogexpress'); ?></option>
                                    <option <?php if( $twp_disable_ajax_load_next_post == 'theme-normal-navigation' ){ echo 'selected'; } ?> value="theme-normal-navigation"><?php esc_html_e('Next Previous Navigation','blogexpress'); ?></option>
                                    <option <?php if( $twp_disable_ajax_load_next_post == 'ajax-next-post-load' ){ echo 'selected'; } ?> value="ajax-next-post-load"><?php esc_html_e('Ajax Load Next 3 Posts Contents','blogexpress'); ?></option>

                                </select>

                            </div>

                        </div>

                    </div>

                <?php endif;

                $blogexpress_ed_post_views = esc_html( get_post_meta( $post->ID, 'blogexpress_ed_post_views', true ) );
                $blogexpress_ed_post_read_time = esc_html( get_post_meta( $post->ID, 'blogexpress_ed_post_read_time', true ) );
                $blogexpress_ed_post_like_dislike = esc_html( get_post_meta( $post->ID, 'blogexpress_ed_post_like_dislike', true ) );
                $blogexpress_ed_post_author_box = esc_html( get_post_meta( $post->ID, 'blogexpress_ed_post_author_box', true ) );
                $blogexpress_ed_post_social_share = esc_html( get_post_meta( $post->ID, 'blogexpress_ed_post_social_share', true ) );
                $blogexpress_ed_post_reaction = esc_html( get_post_meta( $post->ID, 'blogexpress_ed_post_reaction', true ) );
                $blogexpress_ed_post_rating = esc_html( get_post_meta( $post->ID, 'blogexpress_ed_post_rating', true ) ); ?>

                <div id="twp-tab-booster-content" class="metabox-content-wrap">

                    <div class="metabox-opt-panel">

                        <h3 class="meta-opt-title"><?php esc_html_e('Booster Extension Plugin Content','blogexpress'); ?></h3>

                        <div class="metabox-opt-wrap twp-checkbox-wrap">

                            <input type="checkbox" id="blogexpress-ed-post-views" name="blogexpress_ed_post_views" value="1" <?php if( $blogexpress_ed_post_views ){ echo "checked='checked'";} ?>/>
                            <label for="blogexpress-ed-post-views"><?php esc_html_e( 'Disable Post Views','blogexpress' ); ?></label>

                        </div>

                        <div class="metabox-opt-wrap twp-checkbox-wrap">

                            <input type="checkbox" id="blogexpress-ed-post-read-time" name="blogexpress_ed_post_read_time" value="1" <?php if( $blogexpress_ed_post_read_time ){ echo "checked='checked'";} ?>/>
                            <label for="blogexpress-ed-post-read-time"><?php esc_html_e( 'Disable Post Read Time','blogexpress' ); ?></label>

                        </div>

                        <div class="metabox-opt-wrap twp-checkbox-wrap">

                            <input type="checkbox" id="blogexpress-ed-post-like-dislike" name="blogexpress_ed_post_like_dislike" value="1" <?php if( $blogexpress_ed_post_like_dislike ){ echo "checked='checked'";} ?>/>
                            <label for="blogexpress-ed-post-like-dislike"><?php esc_html_e( 'Disable Post Like Dislike','blogexpress' ); ?></label>

                        </div>

                        <div class="metabox-opt-wrap twp-checkbox-wrap">

                            <input type="checkbox" id="blogexpress-ed-post-author-box" name="blogexpress_ed_post_author_box" value="1" <?php if( $blogexpress_ed_post_author_box ){ echo "checked='checked'";} ?>/>
                            <label for="blogexpress-ed-post-author-box"><?php esc_html_e( 'Disable Post Author Box','blogexpress' ); ?></label>

                        </div>

                        <div class="metabox-opt-wrap twp-checkbox-wrap">

                            <input type="checkbox" id="blogexpress-ed-post-social-share" name="blogexpress_ed_post_social_share" value="1" <?php if( $blogexpress_ed_post_social_share ){ echo "checked='checked'";} ?>/>
                            <label for="blogexpress-ed-post-social-share"><?php esc_html_e( 'Disable Post Social Share','blogexpress' ); ?></label>

                        </div>

                        <div class="metabox-opt-wrap twp-checkbox-wrap">

                            <input type="checkbox" id="blogexpress-ed-post-reaction" name="blogexpress_ed_post_reaction" value="1" <?php if( $blogexpress_ed_post_reaction ){ echo "checked='checked'";} ?>/>
                            <label for="blogexpress-ed-post-reaction"><?php esc_html_e( 'Disable Post Reaction','blogexpress' ); ?></label>

                        </div>

                        <div class="metabox-opt-wrap twp-checkbox-wrap">

                            <input type="checkbox" id="blogexpress-ed-post-rating" name="blogexpress_ed_post_rating" value="1" <?php if( $blogexpress_ed_post_rating ){ echo "checked='checked'";} ?>/>
                            <label for="blogexpress-ed-post-rating"><?php esc_html_e( 'Disable Post Rating','blogexpress' ); ?></label>

                        </div>

                    </div>

                </div>
                
            </div>

        </div>  
            
    <?php }
endif;

// Save metabox value.
add_action( 'save_post', 'blogexpress_save_post_meta' );

if( ! function_exists( 'blogexpress_save_post_meta' ) ):

    function blogexpress_save_post_meta( $post_id ) {

        global $post;

        if ( !isset( $_POST[ 'blogexpress_post_meta_nonce' ] ) || !wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['blogexpress_post_meta_nonce'] ) ), basename( __FILE__ ) ) ){

            return;

        }

        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){

            return;

        }
            
        if ( 'page' == $_POST['post_type'] ) {  

            if ( !current_user_can( 'edit_page', $post_id ) ){  

                return $post_id;

            }

        }elseif( !current_user_can( 'edit_post', $post_id ) ) {

            return $post_id;

        }

        $twp_disable_ajax_load_next_post_old = sanitize_text_field( get_post_meta( $post_id, 'twp_disable_ajax_load_next_post', true ) ); 
        $twp_disable_ajax_load_next_post_new = isset( $_POST['twp_disable_ajax_load_next_post'] ) ? blogexpress_sanitize_meta_pagination( wp_unslash( $_POST['twp_disable_ajax_load_next_post'] ) ) : '' ;
        if( $twp_disable_ajax_load_next_post_new && $twp_disable_ajax_load_next_post_new != $twp_disable_ajax_load_next_post_old ){

            update_post_meta ( $post_id, 'twp_disable_ajax_load_next_post', $twp_disable_ajax_load_next_post_new );

        }elseif( '' == $twp_disable_ajax_load_next_post_new && $twp_disable_ajax_load_next_post_old ) {

            delete_post_meta( $post_id,'twp_disable_ajax_load_next_post', $twp_disable_ajax_load_next_post_old );

        }

        $blogexpress_ed_feature_image_old = absint( get_post_meta( $post_id, 'blogexpress_ed_feature_image', true ) );
        $blogexpress_ed_feature_image_new = isset( $_POST['blogexpress_ed_feature_image'] ) ? absint( wp_unslash( $_POST['blogexpress_ed_feature_image'] ) ) : '';

        if ( $blogexpress_ed_feature_image_new && $blogexpress_ed_feature_image_new != $blogexpress_ed_feature_image_old ){

            update_post_meta ( $post_id, 'blogexpress_ed_feature_image', $blogexpress_ed_feature_image_new );

        }elseif( '' == $blogexpress_ed_feature_image_new && $blogexpress_ed_feature_image_old ) {

            delete_post_meta( $post_id,'blogexpress_ed_feature_image', $blogexpress_ed_feature_image_old );

        }

        $blogexpress_ed_post_views_old = absint( get_post_meta( $post_id, 'blogexpress_ed_post_views', true ) );
        $blogexpress_ed_post_views_new = isset( $_POST['blogexpress_ed_post_views'] ) ? absint( wp_unslash( $_POST['blogexpress_ed_post_views'] ) ) : '';

        if ( $blogexpress_ed_post_views_new && $blogexpress_ed_post_views_new != $blogexpress_ed_post_views_old ){

            update_post_meta ( $post_id, 'blogexpress_ed_post_views', $blogexpress_ed_post_views_new );

        }elseif( '' == $blogexpress_ed_post_views_new && $blogexpress_ed_post_views_old ) {

            delete_post_meta( $post_id,'blogexpress_ed_post_views', $blogexpress_ed_post_views_old );

        }

        $blogexpress_ed_post_read_time_old = absint( get_post_meta( $post_id, 'blogexpress_ed_post_read_time', true ) );
        $blogexpress_ed_post_read_time_new = isset( $_POST['blogexpress_ed_post_read_time'] ) ? absint( wp_unslash( $_POST['blogexpress_ed_post_read_time'] ) ) : '';

        if ( $blogexpress_ed_post_read_time_new && $blogexpress_ed_post_read_time_new != $blogexpress_ed_post_read_time_old ){

            update_post_meta ( $post_id, 'blogexpress_ed_post_read_time', $blogexpress_ed_post_read_time_new );

        }elseif( '' == $blogexpress_ed_post_read_time_new && $blogexpress_ed_post_read_time_old ) {

            delete_post_meta( $post_id,'blogexpress_ed_post_read_time', $blogexpress_ed_post_read_time_old );

        }

        $blogexpress_ed_post_like_dislike_old = absint( get_post_meta( $post_id, 'blogexpress_ed_post_like_dislike', true ) );
        $blogexpress_ed_post_like_dislike_new = isset( $_POST['blogexpress_ed_post_like_dislike'] ) ? absint( wp_unslash( $_POST['blogexpress_ed_post_like_dislike'] ) ) : '';

        if ( $blogexpress_ed_post_like_dislike_new && $blogexpress_ed_post_like_dislike_new != $blogexpress_ed_post_like_dislike_old ){

            update_post_meta ( $post_id, 'blogexpress_ed_post_like_dislike', $blogexpress_ed_post_like_dislike_new );

        }elseif( '' == $blogexpress_ed_post_like_dislike_new && $blogexpress_ed_post_like_dislike_old ) {

            delete_post_meta( $post_id,'blogexpress_ed_post_like_dislike', $blogexpress_ed_post_like_dislike_old );

        }

        $blogexpress_ed_post_author_box_old = absint( get_post_meta( $post_id, 'blogexpress_ed_post_author_box', true ) );
        $blogexpress_ed_post_author_box_new = isset( $_POST['blogexpress_ed_post_author_box'] ) ? absint( wp_unslash( $_POST['blogexpress_ed_post_author_box'] ) ) : '';

        if ( $blogexpress_ed_post_author_box_new && $blogexpress_ed_post_author_box_new != $blogexpress_ed_post_author_box_old ){

            update_post_meta ( $post_id, 'blogexpress_ed_post_author_box', $blogexpress_ed_post_author_box_new );

        }elseif( '' == $blogexpress_ed_post_author_box_new && $blogexpress_ed_post_author_box_old ) {

            delete_post_meta( $post_id,'blogexpress_ed_post_author_box', $blogexpress_ed_post_author_box_old );

        }

        $blogexpress_ed_post_social_share_old = absint( get_post_meta( $post_id, 'blogexpress_ed_post_social_share', true ) );
        $blogexpress_ed_post_social_share_new = isset( $_POST['blogexpress_ed_post_social_share'] ) ? absint( wp_unslash( $_POST['blogexpress_ed_post_social_share'] ) ) : '';

        if ( $blogexpress_ed_post_social_share_new && $blogexpress_ed_post_social_share_new != $blogexpress_ed_post_social_share_old ){

            update_post_meta ( $post_id, 'blogexpress_ed_post_social_share', $blogexpress_ed_post_social_share_new );

        }elseif( '' == $blogexpress_ed_post_social_share_new && $blogexpress_ed_post_social_share_old ) {

            delete_post_meta( $post_id,'blogexpress_ed_post_social_share', $blogexpress_ed_post_social_share_old );

        }

        $blogexpress_ed_post_reaction_old = absint( get_post_meta( $post_id, 'blogexpress_ed_post_reaction', true ) );
        $blogexpress_ed_post_reaction_new = isset( $_POST['blogexpress_ed_post_reaction'] ) ? absint( wp_unslash( $_POST['blogexpress_ed_post_reaction'] ) ) : '';

        if ( $blogexpress_ed_post_reaction_new && $blogexpress_ed_post_reaction_new != $blogexpress_ed_post_reaction_old ){

            update_post_meta ( $post_id, 'blogexpress_ed_post_reaction', $blogexpress_ed_post_reaction_new );

        }elseif( '' == $blogexpress_ed_post_reaction_new && $blogexpress_ed_post_reaction_old ) {

            delete_post_meta( $post_id,'blogexpress_ed_post_reaction', $blogexpress_ed_post_reaction_old );

        }

        $blogexpress_ed_post_rating_old = absint( get_post_meta( $post_id, 'blogexpress_ed_post_rating', true ) );
        $blogexpress_ed_post_rating_new = isset( $_POST['blogexpress_ed_post_rating'] ) ? absint( wp_unslash( $_POST['blogexpress_ed_post_rating'] ) ) : '';

        if ( $blogexpress_ed_post_rating_new && $blogexpress_ed_post_rating_new != $blogexpress_ed_post_rating_old ){

            update_post_meta ( $post_id, 'blogexpress_ed_post_rating', $blogexpress_ed_post_rating_new );

        }elseif( '' == $blogexpress_ed_post_rating_new && $blogexpress_ed_post_rating_old ) {

            delete_post_meta( $post_id,'blogexpress_ed_post_rating', $blogexpress_ed_post_rating_old );

        }

    }

endif;   