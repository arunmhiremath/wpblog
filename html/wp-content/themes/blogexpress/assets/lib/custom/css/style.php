<?php
/**
 * BlogExpress Dynamic Styles
 *
 * @package BlogExpress
 */

function blogexpress_dynamic_css()
{

    $blogexpress_default = blogexpress_get_default_theme_options();
    $blogexpress_primary_color = $blogexpress_default['blogexpress_primary_color'];
    $blogexpress_border_color = $blogexpress_default['blogexpress_border_color'];
    $background_color = get_theme_mod('background_color', $blogexpress_default['blogexpress_background_color']);
    $background_color = '#' . str_replace("#", "", $background_color);


    echo "<style type='text/css' media='all'>"; ?>


    body,
    .offcanvas-wraper,
    .header-searchbar-inner{
    background-color: <?php echo esc_attr($background_color); ?>;
    }

    aaaaaaaaaa{
    background-color: <?php echo esc_attr($blogexpress_primary_color); ?>;
    }

    aaaaaaaaaa{
    border-color: <?php echo esc_attr($blogexpress_border_color); ?>;
    }



    <?php echo "</style>";
}

add_action('wp_head', 'blogexpress_dynamic_css', 100);