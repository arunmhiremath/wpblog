<?php
/**
 * Search Template for search form
 *
 * @package WordPress
 * @subpackage BlogExpress
 * @since 1.0.0
 */

$blogexpress_default = blogexpress_get_default_theme_options();
$blogexpress_header_layout = get_theme_mod( 'blogexpress_header_layout', $blogexpress_default['blogexpress_header_layout'] );

if( $blogexpress_header_layout == 'layout-1' ){ ?>

    <form role="search" method="get" class="search-form search-form-custom" action="<?php echo esc_url(home_url('/')); ?>">

        <label>
            <input type="search" class="search-field" placeholder="<?php esc_attr_e('Search …', 'blogexpress'); ?>" value="<?php echo esc_attr(get_search_query()) ?>" name="s">
        </label>
        <button type="submit" class="search-submit"><?php blogexpress_the_theme_svg('search'); ?></button>
    </form>

<?php
}else{ ?>

	<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<label>
			<span class="screen-reader-text"><?php esc_html_e('Search for:','blogexpress'); ?></span>
			<input type="search" class="search-field" placeholder="<?php esc_attr_e('Search …','blogexpress'); ?>" value="<?php echo esc_attr( get_search_query() ) ?>" name="s">
		</label>
		<input type="submit" class="search-submit" value="<?php esc_attr_e('Search','blogexpress'); ?>">
	</form>

<?php
}
