== BlogExpress ==

Contributors: Themeinwp

Tested up to: 5.7
Requires PHP: 5.5
Stable tag: 1.0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Modern, clean, minimal, and elegant, BlogExpress is a responsive WordPress Theme suitable for any kind of blog, personal, travel, fashion, food, photography, publishing, or tutorial blog sites. BlogExpress WordPress theme is perfect for creatives, service-based businesses, and bloggers who went to tell their story with a bold, minimalist style.

== Copyright ==

BlogExpress WordPress Theme, Copyright 2021 Themeinwp
BlogExpress is distributed under the terms of the GNU GPL.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.


== Credits ==

normalize:
Author: 2012-2015 Nicolas Gallagher and Jonathan Neal
Source: http://necolas.github.io/normalize.css
License: [MIT/GPL2 Licensed](http://opensource.org/licenses/MIT)

BreadcrumbTrail:
Author: Justin Tadlock
Source: http://themehybrid.com/plugins/breadcrumb-trail
License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

Waypoints:
Author: Caleb Troughton
Source: https://github.com/imakewebthings/waypoints/blob/master/licenses.txt
License: Licensed under the MIT license

Swiper:
Author: Vladimir Kharlampidi
Source: https://swiperjs.com
License: Released under the MIT License

Tilt:
Author: Gijs Rogé
Source: https://github.com/gijsroge/tilt.js/blob/master/LICENSE
License: Released under the MIT License

TGM-Plugin-Activation:
Author: Thomas Griffin (thomasgriffinmedia.com)
URL:  https://github.com/TGMPA/TGM-Plugin-Activation
License: GNU General Public License, version 2

Webfont Loader:
Copyright: Copyright (c) 2020 WPTT
URL:  https://github.com/WPTT/webfont-loader
License: MIT License 

== Code from Twenty Nineteen ==
Copyright (c) 2018-2019 WordPress.org
License: GPLv2
Source: https://wordpress.org/themes/twentyninteen/
Included as part of the following classes and functions:
- BlogExpress_SVG_Icons

== Image Used ==
Copyright Austin Neill
License: CC0 1.0 Universal (CC0 1.0)
License URL: https://stocksnap.io/license
Source: https://stocksnap.io/photo/concert-singer-F66MXRQS1K

Copyright Candace McDaniel
License: CC0 1.0 Universal (CC0 1.0)
License URL: https://stocksnap.io/license
Source: https://stocksnap.io/photo/waves-crashing-QR6RZJIMOS

Copyright Yoga Mom
License: CC0 1.0 Universal (CC0 1.0)
License URL: https://stocksnap.io/license
Source: https://stocksnap.io/photo/mother-yoga-7ANDXK5NLK

Copyright Macro Mama
License: CC0 1.0 Universal (CC0 1.0)
License URL: https://stocksnap.io/license
Source: https://stocksnap.io/photo/macro-flower-OBVAEJPBMF

== Changelog ==

= 1.0.0 =
* Initial release.

= 1.0.1 =
* POT file update.
* jquery.waypoint.js file added.
* Random issue fixed. 