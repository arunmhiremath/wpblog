<?php
/**
 * Header file for the BlogExpress WordPress theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage BlogExpress
 * @since 1.0.0
 */
?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php
if( function_exists('wp_body_open') ){
    wp_body_open();
}
$blogexpress_default = blogexpress_get_default_theme_options();
$ed_preloader = get_theme_mod( 'ed_preloader', $blogexpress_default['ed_preloader'] ); ?>

<?php if( $ed_preloader ){ ?>

    <div class="preloader hide-no-js">
        <div class="preloader-wrapper">
            <div class="theme-loader">
                <div class="theme-loading">
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<div class="theme-custom-cursor theme-cursor-primary"></div>
<div class="theme-custom-cursor theme-cursor-secondary"></div>

<div id="blogexpress-page" class="blogexpress-hfeed blogexpress-site">
<a class="skip-link screen-reader-text" href="#site-content"><?php esc_html_e('Skip to the content', 'blogexpress'); ?></a>

<?php
$blogexpress_header_top_ed = get_theme_mod( 'blogexpress_header_top_ed', $blogexpress_default['blogexpress_header_top_ed'] );
if( $blogexpress_header_top_ed ){
    get_template_part( 'template-parts/header/header', 'top' );
}

$blogexpress_header_layout = get_theme_mod( 'blogexpress_header_layout', $blogexpress_default['blogexpress_header_layout'] );
get_template_part( 'template-parts/header/header', $blogexpress_header_layout );

if( !is_paged() && ( is_home() || is_front_page() ) ){

    blogexpress_main_slider();
    blogexpress_category_carousel();
    
} ?>

<div id="content" class="site-content">
