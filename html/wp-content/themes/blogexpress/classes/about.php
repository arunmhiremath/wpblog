<?php

/**
 * BlogExpress About Page
 * @package BlogExpress
 *
*/

if( !class_exists('BlogExpress_About_page') ):

	class BlogExpress_About_page{

		function __construct(){

			add_action('admin_menu', array($this, 'blogexpress_backend_menu'),999);

		}

		// Add Backend Menu
        function blogexpress_backend_menu(){

            add_theme_page(esc_html__( 'BlogExpress Options','blogexpress' ), esc_html__( 'BlogExpress Options','blogexpress' ), 'activate_plugins', 'blogexpress-about', array($this, 'blogexpress_main_page'));

        }

        // Settings Form
        function blogexpress_main_page(){

            require get_template_directory() . '/classes/about-render.php';

        }

	}

	new BlogExpress_About_page();

endif;