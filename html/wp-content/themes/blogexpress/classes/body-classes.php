<?php
/**
* Body Classes.
*
* @package BlogExpress
*/
 
 if (!function_exists('blogexpress_body_classes')) :

    function blogexpress_body_classes($classes) {

        $blogexpress_default = blogexpress_get_default_theme_options();
        global $post;
        // Adds a class of hfeed to non-singular pages.
        if ( !is_singular() ) {
            $classes[] = 'hfeed';
        }

        if( is_singular('post') ){

            $blogexpress_ed_post_reaction = esc_attr( get_post_meta( $post->ID, 'blogexpress_ed_post_reaction', true ) );
            if( $blogexpress_ed_post_reaction ){
                $classes[] = 'hide-comment-rating';
            }

        }
        
        return $classes;
    }

endif;

add_filter('body_class', 'blogexpress_body_classes');