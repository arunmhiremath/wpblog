<?php
if ( !class_exists('BlogExpress_Dashboard_Notice') ):

    class BlogExpress_Dashboard_Notice
    {
        function __construct()
        {	
            global $pagenow;

        	if( $this->blogexpress_show_hide_notice() ){

	            add_action( 'admin_notices',array( $this,'blogexpress_admin_notiece' ) );
	        }
	        add_action( 'wp_ajax_blogexpress_notice_dismiss', array( $this, 'blogexpress_notice_dismiss' ) );
			add_action( 'switch_theme', array( $this, 'blogexpress_notice_clear_cache' ) );
        
            if( isset( $_GET['page'] ) && $_GET['page'] == 'blogexpress-about' ){

                add_action('in_admin_header', array( $this,'blogexpress_hide_all_admin_notice' ),1000 );

            }
        }

        public function blogexpress_hide_all_admin_notice(){

            remove_all_actions('admin_notices');
            remove_all_actions('all_admin_notices');

        }
        
        public static function blogexpress_show_hide_notice( $status = false ){

            if( $status ){

                if( (class_exists( 'Booster_Extension_Class' ) ) || get_option('blogexpress_admin_notice') ){

                    return false;

                }else{

                    return true;

                }

            }

            // Check If current Page 
            if ( isset( $_GET['page'] ) && $_GET['page'] == 'blogexpress-about'  ) {
                return false;
            }

        	// Hide if dismiss notice
        	if( get_option('blogexpress_admin_notice') ){
				return false;
			}
        	// Hide if all plugin active
        	if ( class_exists( 'Booster_Extension_Class' ) && class_exists( 'Demo_Import_Kit_Class' ) && class_exists( 'Themeinwp_Import_Companion' ) ) {
				return false;
			}
			// Hide On TGMPA pages
			if ( ! empty( $_GET['tgmpa-nonce'] ) ) {
				return false;
			}
			// Hide if user can't access
        	if ( current_user_can( 'manage_options' ) ) {
				return true;
			}
			
        }

        // Define Global Value
        public static function blogexpress_admin_notiece(){

            ?>
            <div class="updated notice is-dismissible welcome-panel twp-blogexpress-notice">

                <h3><?php esc_html_e('Quick Setup','blogexpress'); ?></h3>

                <p><strong><?php esc_html_e('BlogExpress is now installed and ready to use. Are you looking for a better experience to set up your site?','blogexpress'); ?></strong></p>

                <small><?php esc_html_e("We've prepared a unique onboarding process through our",'blogexpress'); ?> <a href="<?php echo esc_url( admin_url().'themes.php?page='.get_template().'-about') ?>"><?php esc_html_e('Getting started','blogexpress'); ?></a> <?php esc_html_e("page. It helps you get started and configure your upcoming website with ease. Let's make it shine!",'blogexpress'); ?></small>

                <p>
                    <a class="button button-primary twp-install-active" href="javascript:void(0)"><?php esc_html_e('Install and activate recommended plugins','blogexpress'); ?></a>
                    <span class="quick-loader-wrapper"><span class="quick-loader"></span></span>

                    <a target="_blank" class="button button-primary" href="<?php echo esc_url( 'https://preview.themeinwp.com/blogexpress/' ); ?>"><?php esc_html_e('View Demo','blogexpress'); ?></a>
                    <a target="_blank" class="button button-primary" href="<?php echo esc_url( 'https://www.themeinwp.com/theme/blogexpress-pro/' ); ?>"><?php esc_html_e('Upgrade to Pro','blogexpress'); ?></a>
                    <a class="btn-dismiss twp-custom-setup" href="javascript:void(0)"><?php esc_html_e('Dismiss this notice.','blogexpress'); ?></a>

                </p>

            </div>

        <?php
        }

        public function blogexpress_notice_dismiss(){

        	if ( isset( $_POST[ '_wpnonce' ] ) && wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST[ '_wpnonce' ] ) ), 'blogexpress_ajax_nonce' ) ) {

	        	update_option('blogexpress_admin_notice','hide');

	        }

            die();

        }

        public function blogexpress_notice_clear_cache(){

        	update_option('blogexpress_admin_notice','');

        }

    }
    new BlogExpress_Dashboard_Notice();
endif;