=== Awe Blog ===

Contributors: flawlesstheme
Tags: one-column, two-columns, right-sidebar, custom-background, custom-logo, custom-menu, editor-style, featured-images, post-formats, theme-options, threaded-comments, translation-ready
Requires at least: 4.5
Tested up to: 5.4
Requires PHP: 5.6
Stable tag: 1.0.6
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A starter theme called Awe Blog.

Awe Blog WordPress Theme, Copyright 2021 flawlesstheme
Awe Blog is distributed under the terms of the GNU GPL

== Description ==

Awe Blog is an elegeantly designed Blog WordPress Theme and has a classic blog layout with sidebar on the right, big animated slider.This is not just another boring looking Blog theme,The homepage is clean and elegant, soft and readable typography, along with plenty of whitespace.If you’re looking for a blog theme that places a focus on your images, as well as text, then Awe Blog is worth a test drive.For bloggers on a budget, this simple design is one of the best free responsive WordPress themes to check out.Browse the demo : https://demo.flawlessthemes.com/awe-blog-free/ 



== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =



Awe Blog includes support for WooCommerce and for Infinite Scroll in Jetpack.



== Changelog ==

= 1.0.0 - 11 June 2021 =
* Initial release

= 1.0.1 - 12 June 2021 =
* Minor Issues fixed

= 1.0.2 - 12 June 2021 =
* Minor Issues fixed

= 1.0.4 - 16 June 2021 =
* Minor Issues fixed

= 1.0.5 - 17 June 2021 =
* Minor Issues fixed

= 1.0.6 - 21 June 2021 =
* Minor Issues fixed






== Credits ==

* Based on Underscores https://underscores.me/, (C) 2012-2020 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2018 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)



== Screenshots ==

Images in screenshot are all CCO licensed (Creative Commons Zero (CC0) license)


Image for theme screenshot, Credit pxhere
License: Creative Commons Zero (CC0) license
License URL: https://pxhere.com/en/license
Source: https://pxhere.com/en/photo/1633009

Image for theme screenshot, Credit pxhere
License: Creative Commons Zero (CC0) license
License URL: https://pxhere.com/en/license
Source: https://pxhere.com/en/photo/100653

Image for theme screenshot, Credit pxhere
License: Creative Commons Zero (CC0) license
License URL: https://pxhere.com/en/license
Source: https://pxhere.com/en/photo/1637638

Image for theme screenshot, Credit pxhere
License: Creative Commons Zero (CC0) license
License URL: https://pxhere.com/en/license
Source: https://pxhere.com/en/photo/1418982

Image for theme screenshot, Credit pxhere
License: Creative Commons Zero (CC0) license
License URL: https://pxhere.com/en/license
Source: https://pxhere.com/en/photo/1232985




== Fonts ==

Ionicons

License : The MIT License (MIT)
Source: http://ionicons.com/


Font Awesome - http://fontawesome.io

Licenses: SIL OFL 1.1, MIT
Source: https://fortawesome.github.io/Font-Awesome/




== Styles ==

Bootstrap v3.3.7, Copyright 2011-2016 Twitter, Inc.
Licenses: MIT
Source: http://getbootstrap.com

Slick css 1.6.0, Copyright (c) 2014 Ken Wheeler
Licenses: MIT
Source: http://github.com/kenwheeler/slick

Font Awesome 4.6.3
Licenses: Font: SIL OFL 1.1, CSS: MIT License
Source: https://fortawesome.github.io/Font-Awesome/

Ionicons, v1.5.2
Licenses: MIT License: https://github.com/driftyco/ionicons
Source: http://ionicons.com/

youtube popup v1.0.1
Licenses: MIT and GPL licenses
Source: http://wp-time.com/youtube-popup-jquery-plugin/



== Scripts ==

Bootstrap v3.3.7, Copyright 2011-2015 Twitter, Inc.
Licenses: MIT
Source: http://getbootstrap.com


Slick.js 1.6.0, Copyright (c) 2014 Ken Wheeler
Licenses: MIT
Source: http://github.com/kenwheeler/slick

youtube popup v1.0.1
Licenses: MIT and GPL licenses
Source: http://wp-time.com/youtube-popup-jquery-plugin/
